# jsh

an automatic javascript syntax highlighter for code and pre tags


# including the files

insert this right before `</body>`:

```html
<script src="/path/to/jsh.sh"></script>
</body>
```

Then select a theme, include this between the `<head>` tags

```html
<head>
	<link rel="stylesheet" type="text/css" href="styles/xterm-light.css">
</head>
```

# supported languages

- C &rarr; -> class="c"
- PHP &rarr; class="php"
- SQL &rarr; class="sql"
- Markdown &rarr; class="md"
- HTML &rarr; class="html"
- Shell &rarr; class="sh"

# Example

```html
<pre><code class="c">
#include <stdio.h>

int main(int argc, char *argv[]){
	// start at 2 to skip proc's name
	int i=2;
	for(i; i<=argc; i++){
		printf("%s\n", argv[i-1]);
	}

	return 0;
}
</code></pre>
```

becomes: 

![syntax highlight demo](demo.png)

# Adding languages
it's all regex. Submit a MR

# Adding themes
it's all CSS variables. Submit a MR
